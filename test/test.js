var assert = require("assert").strict;
var webdriver = require("selenium-webdriver");
require("chromedriver");

const serverUri = "http://localhost:3000/";
const appTitle = "React Selenium App";

var driver = new webdriver.Builder().forBrowser('chrome').build();

driver.get(serverUri);

describe('login Flow',async function () {
    it("Should load the home page and get title",async function() {
  return new Promise(async (resolve, reject) => {
   await driver.wait(webdriver.until.elementLocated({name:"loginButton"})).click()
   await resolve()

    .catch(err => reject(err));
  });
 });
 it('should enter values',async function(){
     return new Promise(async(resolve,reject) => {
        await driver.wait(webdriver.until.urlContains(serverUri+"login"));
       await driver.findElement(webdriver.By.name('username')).sendKeys('a')
        await driver.findElement(webdriver.By.name('password')).sendKeys('a')
        await driver.wait(webdriver.until.elementLocated({name : 'submitButton'})).click()

         await resolve()
        .catch(err => reject(err));
     })
 });
 it('closes the window',async function(){
     return new Promise(async(resolve,reject)=>{
        await driver.wait(webdriver.until.urlContains(serverUri+"chat"));
        await driver.quit()
         await resolve()
         .catch(err => reject(err));
     })
 })
})

// driver.wait(webdriver.until.elementLocated({name:"loginButton"})).click();
// driver.wait(webdriver.until.urlContains(serverUri+"login"));
// driver.wait(webdriver.until.elementLocated({xpath:'//*[@id="root"]/div/body/div/a[2]/img'}));
// driver.wait(webdriver.until.elementLocated({xpath:'//*[@id="root"]/div/body/div/div[2]/input'})).sendKeys('a');
// driver.wait(webdriver.until.elementLocated({xpath:'//*[@id="root"]/div/body/div/div[3]/input'})).sendKeys('a');
// driver.findElement(webdriver.By.name('username')).sendKeys('a');
// driver.findElement(webdriver.By.name('password')).sendKeys('a');
// driver.wait(webdriver.until.elementLocated({xpath:'//*[@id="root"]/div/body/div/button'})).click();
// driver.findElement(webdriver.By.name('loginButton')).sendKeys(webdriver.Key.ENTER);
// driver.findElement(webdriver.By.name('username')).sendKeys('a');
// driver.findElement(webdriver.By.name('password')).sendKeys('a');
// driver.findElement(webdriver.By.name('submitButton')).sendKeys(webdriver.Key.ENTER);
