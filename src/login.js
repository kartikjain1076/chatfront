/*eslint-disable*/
import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Snackbar from '@material-ui/core/Snackbar';


class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      username : '',
      password : '',
      open : false,
      open2 : false
    }
  }

  changeData = (e) => {
    this.setState({[e.target.name] : e.target.value});
  }

  handleClose = () => {
    this.setState({open : false})
  }

  componentDidMount = () => {
    if(localStorage.username !== undefined)
    this.props.history.push('/chat');
  }

  submitHandler = (e) => {
    console.log('sdf');
    e.preventDefault();
    let option = {
      headers : {'Accept' : 'application/json','Content-Type':'application/json'},
      method : 'Post',
      body : JSON.stringify(this.state)
    }
    fetch("https://kartikchatback.herokuapp.com/login",option)
    .then((response)=>{
      console.log(response);
      if(response.status===200)
      {
        response.text().then((action)=>
        {
          console.log(action);
          if(typeof(JSON.parse(action)) == "object")
                {
                    let a = JSON.parse(action);
                    if(a.res === 'ok')
                    {
                      if(a.body[0].verified)
                      {
                        localStorage.username = a.body[0].username;
                        localStorage.password = a.body[0].password;
                        localStorage._id = a.body[0]._id;
                        this.props.history.push('/chat');
                      }
                      else
                      {
                        this.setState({open2 : true})
                      }
                    }
                    else
                    {
                      this.setState({open : true})
                    }
                }
          else
          {
            console.log('err');
          }
        })
      }
    })
    .catch((err)=>{  
      console.log('Fetch Error :-S', err);
      if(err)
        this.props.history.push('/error');  
    });
  }

  render() {
    return (
      <div className="App">
          
      <link rel="stylesheet" href="css/style.css" />
      
      <body>
      
        
      <div class="panel-lite">
        <div class="thumbur">
          <div class="icon-lock"></div>
        </div>
        <h4>Login</h4>
        
        <div class="form-group">
        <input class="form-control" name = 'username' required="required" onChange={this.changeData} />
        <label class="form-label">Username    </label>
      </div>
      <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={this.state.open}
          autoHideDuration={3000}
          message = 'Incorrect Id Password!!!'
          onClose = {this.handleClose}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={this.state.open2}
          autoHideDuration={3000}
          message = 'Verify Your EMail !!!'
          onClose = {this.handleClose}
        />
        <div class="form-group">
          <input class="form-control" name = 'password' type="password" required="required" onChange={this.changeData} />
          <label class="form-label">Password</label>
        </div><a href="#"><Link to ='/'>Register</Link>  </a><br />
        <a href="https://kartikchatback.herokuapp.com/oauth/google"><img src='google.png' style = {{width : '300px', height : '50px'}} /></a>
        <button class="floating-btn" onClick={this.submitHandler} name='submitButton'><i class="icon-arrow"></i></button>
      </div>
        
        
      
      </body>
      </div>
    );
  }
}

export default Login;
