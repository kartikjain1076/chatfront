import io from 'socket.io-client';

let socket_id;

export const socketconnection = io.connect('https://kartikchatback.herokuapp.com');
socketconnection.on('connect',function(){    
    socket_id = socketconnection.id;
    console.log(socket_id);
});  
export const userConnect = data => {
    socketconnection.emit('userConnect' , data);
};
export const userConnected = cb => {
    socketconnection.on('userConnected',function(data){
        cb(data);
    });
};
export const userDisconnected = cb => {
    socketconnection.on('userDisconnected',function(data){
        cb(data);
    })
};
export const sendMessage = data => {
    socketconnection.emit('sendMessage', data);
};
export const getMessage = cb => {
    socketconnection.on('getMessage',function(data){
        cb(data);
    })
};
export const sendRead = data => {
    socketconnection.emit('sendRead', data);
};
export const sentRead = cb => {
    socketconnection.on('sentRead',function(data){
        cb(data);
    })
};
export const createGroup = data => {
    socketconnection.emit('createGroup', data);
};
export const groupCreated = cb => {
    socketconnection.on('groupCreated',function(data){
        cb(data);
    })
};
export const disconnectIt = data => {
    socketconnection.emit('disconnectIt', data);
};

export default socketconnection;